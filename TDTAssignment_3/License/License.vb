REM License issued by BayesFusion Licensing Server
REM This code must be executed before any other SMILE.NET object is created 
Dim smileLicense As New Smile.License(
	"SMILE LICENSE 48404107 1448dee3 528e5beb " +
	"THIS IS AN ACADEMIC LICENSE AND CAN BE USED " +
	"SOLELY FOR ACADEMIC RESEARCH AND TEACHING, " +
	"AS DEFINED IN THE BAYESFUSION ACADEMIC " +
	"SOFTWARE LICENSING AGREEMENT. " +
	"Serial #: d99hljvjmwar2ps6h7bkng9y0 " +
	"Issued for: Otilie Arnesen (otilie.arnesen@gmail.com) " +
	"Academic institution: NTNU in Gj"+ChrW(&H00f8)+"vik Norway " +
	"Valid until: 2021-09-06 " +
	"Issued by BayesFusion activation server",
	{
	&Hdd,&H59,&Hc4,&Hc6,&H50,&H54,&Hd4,&Hfa,&H40,&H3f,&H9a,&H34,&H2f,&Hdd,&H87,&H1c,
	&H44,&H7f,&Hc7,&H06,&H3e,&H18,&Hdd,&Hca,&He2,&Had,&H8f,&H55,&H11,&H0d,&H93,&Hab,
	&H0f,&Hdd,&H51,&H12,&H9d,&Hc7,&Hdb,&Hd0,&H4d,&H58,&Hb1,&H3d,&H4c,&H3b,&H06,&H37,
	&H31,&H58,&H03,&H81,&H49,&H7c,&Hd1,&H74,&H69,&H65,&H7b,&Hc2,&H1e,&Hf0,&Hb2,&H01
	})
