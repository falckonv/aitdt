// License issued by BayesFusion Licensing Server
// This code must be executed before any other jSMILE object is created
new smile.License(
	"SMILE LICENSE 48404107 1448dee3 528e5beb " +
	"THIS IS AN ACADEMIC LICENSE AND CAN BE USED " +
	"SOLELY FOR ACADEMIC RESEARCH AND TEACHING, " +
	"AS DEFINED IN THE BAYESFUSION ACADEMIC " +
	"SOFTWARE LICENSING AGREEMENT. " +
	"Serial #: d99hljvjmwar2ps6h7bkng9y0 " +
	"Issued for: Otilie Arnesen (otilie.arnesen@gmail.com) " +
	"Academic institution: NTNU in Gj\u00f8vik Norway " +
	"Valid until: 2021-09-06 " +
	"Issued by BayesFusion activation server",
	new byte[] {
	-35,89,-60,-58,80,84,-44,-6,64,63,-102,52,47,-35,-121,28,
	68,127,-57,6,62,24,-35,-54,-30,-83,-113,85,17,13,-109,-85,
	15,-35,81,18,-99,-57,-37,-48,77,88,-79,61,76,59,6,55,
	49,88,3,-127,73,124,-47,116,105,101,123,-62,30,-16,-78,1
	}
);
