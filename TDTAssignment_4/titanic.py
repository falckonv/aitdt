"""
Code to load and run AIMA(2020) pseudo code 19.5
LEARN_DECISION_TREE()

Using the dataset titanic as train.csv and test.csv

"""
import copy
from collections import defaultdict
from statistics import stdev
import numpy as np
import pandas as pd
import random as rn
import matplotlib.pyplot as plt
import networkx as nx
from graphviz import Digraph
import pygraphviz as pgv


def DataPrepareCategoricalMinimal():
    # Importing training data
    df = pd.read_csv("titanic/train.csv")
    # Drop all non-numeric categorical features
    df = df.drop(columns=["Name", "Ticket", "Fare", "Age", "Cabin", "Sex", "Embarked"])
    # Rename the dependent variable Y to name "Target"
    df = df.rename(columns={"Survived":"Target"})
    
    # TODO!!!!
    # Form analysing the data, we discover that the variable "Parch" with value = 6 has no
    # examples for one of the Y (target-category) - with vectors this result in a 5 versus
    # 6 size. This result in a small calculation error, as I currently have not found
    # out how to populate empty cells with 0 instead of pandas truncating the arrays.
    # As examples are cut down, the risk for this is great for more variables, I have had no 
    # time to adjust for this (making it risky to use arrays and matrixes for cross-calulations)
    # A hard coded function for "Parch" is in the jupyter-notebook as CheckValueCategories()
    # It is not included here
   
    assert type(df) is pd.core.frame.DataFrame, "Could not assert pandas dataframe"
    assert df.isnull().sum().any() == 0,    "Failed to assert no empty cells (isnull)"
    assert df.isna().sum().any() == 0,      "Failed to assert no empty cells (isna)"

    print(df.nunique())
    

    return df


class Leaf:
    def __init__(self, category, attribute=None):
        self.category  = category
        self.attribute = attribute
        
    def __call__(self, example, attribute=None):
        print("__call__ from LEAF")
        return self.category, self.attribute
    
    def __getitem__(self, example, attribute=None):
        return self.category, self.attribute
    
    def __setitem__(self, category, attribute=None):
        return self.category, self.attribute
    
    def __repr__(self):
        return repr(self.category)
    
    def display(self, level=0):
        print("Category is ", self.category)
        #print("Attribute is ", self.attribute)
        

class DecisionTree:
    def __init__(self, dataset):
        self.dataset = dataset
        self.tree = self.LearnDecisionTree(dataset.copy(), [*dataset])
      
    def count_attribute(self, examples=None, a=None, value=None):
        if (not examples):
            return self.dataset[a][self.dataset[a] == value].value_counts().copy()
        assert type(examples) is pd.core.frame.DataFrame, "Could not assert pandas dataframe"
        return examples[a][self.dataset[a] == value].value_counts()
        
    def PluralityValue(self, examples):
        
        att = [*examples]
        
        print("\n\nPluralityValue running.....", att)
        
        for a , e in examples.items():
            maxValue = -99
            if (a == "Target"):
                continue
                
            c = -1
            name = ""
            print("\nEntering the looooooop to extract Y values for ", a)
            # Split on Target (Y) category values
            valList1 = e[examples["Target"] == 1].value_counts().max()
            valList0 = e[examples["Target"] == 0].value_counts().max()
             
            print("ValList1 = ", valList1)
            print("ValList0 = ", valList0)
  
            if (valList1 < valList0):
                valList = valList0
                category = 0
            else:
                valList = valList1
                category = 1
            
            print("ValList ----> ", valList)
            
            if (maxValue < valList):
                print("Found a better candidate!", a)
                maxValue = valList
                name = a
                c = category
                
                # Get the attribute value categories
                arrayV = examples[a].value_counts().sort_index().index.to_numpy()
                for i in range(len(arrayV)):
                    value = arrayV[i]  # Gives the attribute value category [i]
    
                
            # TODO: this is not very effective!!!
            if (maxValue == valList):
                if (rn.random() < 0.5):
                    print("Did randomly choose an equal candidate!", a)
                    name = a
                    c = category
                    
            print("Returned a leaf-node with category ", c)  
            print("and name: ", name)
            print("\n\n")
            return Leaf(a, c)
        
    # BDiscrete() returns q as the probability of a random binary variable being true
    def BDiscrete(self, p, n):
        return p * 1.0/(p + n)

    # Equation on page 662 in AIMA(2020)
    # B(q) is the entropy of a variable with probability of q = true
    def B(self, q):
        return - ((q * np.log2(q)) + ((1 - q) * np.log2(1 - q)))

    # Equation on page 662 in AIMA(2020)
    # ap an an are numpy arrays with values on d items
    def Remainder(self, ap, an, p, n):
        return ((ap + an)/(p + n)) * self.B(self.BDiscrete(ap, an))

    # Equation on page 662 in AIMA(2020) Importance (entropy based)
    # Gain()returns an array with gains of same shape as ap.shape
    # Takes three functions Remainder, BDiscrete and B
    def Gain(self, ap, an, p, n):
        b = self.B(self.BDiscrete(p,n))      # B()        --> a single float
        print("\n\nFrom Gain()\nB(q) for total p and n is: ", b)
        r = self.Remainder(ap, an, p, n)  # Remainder()--> array
        print("REMAINDER is: ", r)
        print("Gain returned is ", b - r)
        # Returns result and the index of our best choice
        return b - r, np.argmax(r)   # b-r --> array of shape (d,1)
    
    # Returns the name of column/row of highest importance = Gain
    def Importance(self, examples, attributes=None):
        # Get the total number of rows with a positive or negative Y-value
        positive = (examples.loc[:,["Target"]].sum()).to_numpy()
        # number of rows - positive Y-class
        negative = examples.shape[0] - positive

        # Get the number for this attribute split up on outcome = Target (0 or 1)
        attribpos = examples[attributes][examples['Target'] == 1]
        attribneg = examples[attributes][examples['Target'] == 0]

        # Returns cleaned numpy arrays (cleaned = same structure on ap and an)
        ap, an = self.CheckValueCategories(attribpos, attribneg, attributes)

        print("Shape attribpos: ", attribpos.shape)
        print("Shape attribneg: ", attribneg.shape)
        print("\nAttrib pos:\n", attribpos.value_counts().sort_index())
        print("\nAttrib neg:\n", attribneg.value_counts().sort_index())
        print("Shape ap: ", ap.shape)
        print("Shape an: ", an.shape)
        print("Type ap: ", type(ap))
        print("\nap --> ", ap)
        print("\nan --> ", an)

        print("\n\nATTRIBUTE CHECKED: ", attributes)
        return self.Gain(ap, an, positive, negative)
    
    # Takes two pandas dataframe or series objects and returns 
    # complete numpy arrays with concurrent p and n
    def CheckValueCategories(self, p, n, attributes):
        tp = p.value_counts().sort_index()
        tn = n.value_counts().sort_index()
        # QUICK AND DIRTY HARDCODING!!
        if (attributes == "Parch" and (len(tn) != len(tp)) and len(tp)>5):
            print("Removed value item 6 from Parch before analysis.")
            tn = tn.drop(6)
        return tp.to_numpy(), tn.to_numpy()


    def LearnDecisionTree(self, examples, attributes=None, parent_examples=(), tree=None):
        print("\n\nSTART LDT()\n\n")
        assert type(examples) is pd.core.frame.DataFrame, "Could not assert pandas dataframe"
        # Check if examples is empty
        if (examples.empty):
            # PluralityValue returns the most common output value among
            # a set of examples, breaking ties randomly
            print("NO EXAMPLES LEFT (from LDT())") 
            return self.luralityValue(parent_examples)

        # Check if all examples have same class (= Y value)
        elif (examples.Target.nunique() == 1):
            # Get the attribute value category (like Pclass is 1,2,3)?
            # Returns the attribute name and its classification as value on Y
            print("Returns a LEAF node with only category on Y", examples.Target.unique()[0])
            # ONLY END NODE with value 0 or 1 on "Survived" (no branch value)
            return Leaf(examples.Target.unique()[0])

        # ...if attributes is empty
        elif (not attributes or (len(attributes) == 1 and attributes[0] == "Target")):
            # Returns column name (attribute) for the first highest value_counts().max()
            print("\n\nNO ATTRIBUTES (from LDT()) - calling PluralityValue(examples)\n\n")
            return self.PluralityValue(examples)
        
        # Do the IMPORTANCE() test (A = argmax over all a in attributes from Importance())
        # A is a matrix with all a values as rows
        a = attributes[1]
        A, ind = self.Importance(examples, a)
        print("A is: ", A)
        print("ind is: ", ind)
        print("Attributes: ", attributes)
        tree = Graph(a, self.dataset[a], self.PluralityValue(examples))
        
                # Quick and dirty hardcoding
        if (len(attributes) == 1 and attributes[0] == "Target"):
            print("THIS SHOULD NEVER HAPPEN!!!")
            attributes.pop(0)
        
        # "Target" is at attributes[0]
        attributes.pop(1)
        print("Attributes after: ", attributes)
        
        for i in range(len(A)):
            value = examples[a].value_counts().sort_index().index.to_numpy()[i]
            print("Attribute is: ", a)
            print("Value is: ", value)
            new_examples = examples.loc[:,attributes][examples[a] == value].copy()
            
            subtree = self.LearnDecisionTree(new_examples, attributes, examples)
            tree.add(value, subtree)
        print("\n\nReturn TREE\n\n")
        return tree

# Represent an attribute with each value-category of the attribute as branches
# The most promising branch is labeled "default_child"
class Graph:
    def __init__(self, attributestest, attributes, nodes=None, default_child=None, edges=None):                
        self.attributes      = attributes
        self.attributestest  = attributestest or attributes  
        self.default_child   = default_child
        self.edges = edges or defaultdict(list)  # maybe not needed?
        self.nodes = nodes or defaultdict(list)  # same as branches
    
    def __call__(self, example):
        """Given an example, classify it using the attribute and the nodes."""
        key = [*example]
        if key in self.nodes:
            return self.nodes[key](example)
        else:
            # return default class when attribute is unknown
            return self.default_child(example)
        
    # NOT USED    
    class Edge:
        def __init__(self, start, end, weight=1):
            self.start  = start
            self.end    = end
            self.weight = weight
                  
    def add(self, val, subtree):
        """Add a node. val is the attributes value category"""
        self.nodes[val] = subtree
        

    def display(self, level=0):
        name = self.attributestest
        print('Test', name)
        for (val, subtree) in self.nodes.items():
            print(' ' * 4 * level, name, '=', val, '==>', end=' ')
            subtree.display(level + 1)
        
                

# DO NOT HAVE TO USE
class Node:
    def __init__(self, attribute=None, attributes=None, default_child=None, parent=None, weight=None, gain=None, index=None, value=None, neighbours=None, leaf=-1):
        self.attribute     = attribute  # Attribute name 
        self.attributes    = attributes or attribute
        self.default_child = default_child      # Attribute name of default child(best)
        self.parent        = parent     # Current parent node
        self.weight        = weight     # Number of examples
        self.gain          = gain       # Gain in float
        self.neighbours    = neighbours or defaultdict(list)
        self.leaf          = leaf       # If not leaf-node, leaf = -1 else category value 
        self.index         = index      # The index of parent that is most promising
        self.value         = value      # The label value on the attribute    
    
    def __call__(self, attribute):
        
        if default_child:
            return self.default_child(attribute)
        else:
            # return default class when attribute is unknown
            print("Have no child, send parent")
            return self.parent(attribute)
        
    
    
    