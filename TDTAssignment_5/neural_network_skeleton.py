# Use Python 3.8 or newer (https://www.python.org/downloads/)
import unittest
# Remember to install numpy (https://numpy.org/install/)!
import numpy as np
import pickle
import os
from collections import defaultdict  # added


class NeuralNetwork:
    """Implement/make changes to places in the code that contains #TODO."""

    def __init__(self, input_dim: int, hidden_layer: bool) -> None:
        """
        Initialize the feed-forward neural network with the given arguments.
        :param input_dim: Number of features in the dataset.
        :param hidden_layer: Whether or not to include a hidden layer.
        :return: None.
        """

        # --- PLEASE READ --
        # Use the parameters below to train your feed-forward neural network.

        # Number of hidden units if hidden_layer = True.
        self.hidden_units = 25
        

        # This parameter is called the step size, also known as the learning rate (lr).
        # See 18.6.1 in AIMA 3rd edition (page 719).
        # This is the value of α on Line 25 in Figure 18.24.
        #self.lr = 1e-3
        self.lr = 0.05

        # Line 6 in Figure 18.24 says "repeat".
        # This is the number of times we are going to repeat. This is often known as epochs.
        self.epochs = 5000

        # We are going to store the data here.
        # Since you are only asked to implement training for the feed-forward neural network,
        # only self.x_train and self.y_train need to be used. You will need to use them to implement train().
        # The self.x_test and self.y_test is used by the unit tests. Do not change anything in it.
        self.x_train, self.y_train = None, None
        self.x_test, self.y_test = None, None

        # TODO: Make necessary changes here. For example, assigning the arguments "input_dim" and "hidden_layer" to
        # variables and so forth.
        
        self.input_dim    = input_dim     # the number of features in dataset
        self.hidden_layer = hidden_layer  # if using hidden layers (default false)
        self.output_dim   = 1
        self.parameters   = self.initializeParameters(self.input_dim, self.hidden_units, self.output_dim)
        

    def load_data(self, file_path: str = os.path.join(os.getcwd(), 'data_breast_cancer.p')) -> None:
        """
        Do not change anything in this method.

        Load data for training and testing the model.
        :param file_path: Path to the file 'data_breast_cancer.p' downloaded from Blackboard. If no arguments is given,
        the method assumes that the file is in the current working directory.

        The data have the following format.
                   (row, column)
        x: shape = (number of examples, number of features)
        y: shape = (number of examples)
        """
        with open(file_path, 'rb') as file:
            data = pickle.load(file)
            self.x_train, self.y_train = data['x_train'], data['y_train']
            self.x_test, self.y_test = data['x_test'], data['y_test']

    def train(self) -> None:
        """Run the backpropagation algorithm to train this neural network"""
        # TODO: Implement the back-propagation algorithm outlined in Figure 18.24 (page 734) in AIMA 3rd edition.
        # Only parts of the algorithm need to be implemented since we are only going for one hidden layer.

        # Line 6 in Figure 18.24 says "repeat".
        # We are going to repeat self.epochs times as written in the __init()__ method.

        # Line 27 in Figure 18.24 says "return network". Here you do not need to return anything as we are coding
        # the neural network as a class
        
        ###############################################################################################
        # Implementation
        epochs = self.epochs
        
        X = self.x_train.T
        Y = self.y_train.reshape(self.y_train.shape[0],1).T
        
        m = np.squeeze(self.x_train.shape[0])      # Number of examples in training set
        print("/nSTART Neural network/n/n")
        print("X shape is ", X.shape)
        print("Y shape is ", Y.shape)
        print("m is ", m)
        n = 0
        i = 0
        
        while (n < epochs):
             
            A2, delta = self.forward(X)
            
            # This is not actually needed for the assignment ##########
            # It is only visualising progress (does not do anything)
            cost = self.cost(A2)
            # Print the cost every 100 iterations
            if cost and i % 100 == 0:
                print ("Cost after iteration %i: %f" %(i, cost))
            ###########################################################
            
            gradients = self.backward(delta, X, Y, m)
            self.updateParameters(gradients)
                  
            n = n + 1
            i = i + 1
            
    # Runs the complete test set        
    def predictNew(self):
        
        X = self.x_test.T
        Y = self.y_test.reshape(self.y_test.shape[0],1).T
        
        assert(X.shape == (self.x_test.shape[1], self.x_test.shape[0]))
        assert(Y.shape == (1, self.y_test.shape[0]))

        # Computes probabilities using forward propagation, and classifies to 0/1 using 0.5 as the threshold.
        A2, delta = self.forward(X)
        predictions = (A2 > 0.5) 
        print("A2 shape: ", A2.shape)
        print("X in def predict shape: ", X.shape)
        print("predictions shape: ", predictions.shape)
        print ('Accuracy: %d' % float((np.dot(Y,predictions.T) + np.dot(1-Y,1-predictions.T))/float(Y.size)*100) + '%')

        return predictions
    
    
    def forward(self, X):
        
        # Get parameters
        W1 = self.parameters["W1"]  # Hidden
        b1 = self.parameters["b1"]  # Hidden
        W2 = self.parameters["W2"]  # Output
        b2 = self.parameters["b2"]  # Output
        
        # Do the forward pass
        Z1 = np.dot(W1, X) + b1
        A1 = np.tanh(Z1)               # Choose activation method for hidden layer 
                                       # using tanh, but can also be sigmoid or other non-linear
        Z2 = np.dot(W2,A1) + b2
        A2 = self.sigmoid(Z2)          # Choose activation method for output layer: sigmoid
               
        assert(A2.shape == (1, X.shape[1]))  # Just to be sure the data tweaking is ok
        
        # Make L a dictionary with intermediate values to pass to next epoch
        L = {"Z1": Z1,
             "A1": A1,
             "Z2": Z2,
             "A2": A2}
        
        return A2, L
    
    
    def backward(self, delta, X, Y, m):
        
        # Get W1 and W2 from the dictionary parameters
        W1 = self.parameters['W1']
        W2 = self.parameters['W2']

        # Get A1 and A2 from dictionary delta produced in forward()
        A1 = delta['A1']
        A2 = delta['A2']


        # Backward propagation: calculate dW1, db1, dW2, db2. 
        # Updates derived by using chain rule
        dZ2 = A2 - Y
        dW2 = (1/m) * np.dot(dZ2, A1.T)
        db2 = (1/m) * np.sum(dZ2, axis = 1, keepdims= True)
        dZ1 = np.dot(W2.T, dZ2) * (1 - np.power(A1, 2))   
        dW1 = (1/m) * np.dot(dZ1, X.T)
        db1 = (1/m) * np.sum(dZ1, axis = 1 ,keepdims=True)

        gradients = {"dW1": dW1,
                     "db1": db1,
                     "dW2": dW2,
                     "db2": db2}

        return gradients
    
    # Updates parameters using the gradient descent update rule
    def updateParameters(self, grads):
        
        # Get parameters 
        b1 = self.parameters['b1']
        W1 = self.parameters['W1']
        b2 = self.parameters['b2']
        W2 = self.parameters['W2']

        # Get gradients
        dW1 = grads['dW1']
        db1 = grads['db1']
        dW2 = grads['dW2']
        db2 = grads['db2']

        # Update each parameter
        W1 = W1 - self.lr * dW1
        b1 = b1 - self.lr * db1
        W2 = W2 - self.lr * dW2
        b2 = b2 - self.lr * db2
        
        # Update parameters to dictionary
        self.parameters = {"W1": W1,
                      "b1": b1,
                      "W2": W2,
                      "b2": b2}

    
    def sigmoid(self, x):
        return 1/(1+np.exp(-x))  
    
    
    def initializeParameters(self, n_x, n_h, n_y):
        """
        Generates small random floats for the parameter
        dictionary as the starting point
        
        n_x .... number of parameters/features (the labels on the x-varialbles)
        n_h .... number of hidden layers
        n_y .... number of output values (the dependent variable y)
        """
        
        # Initialize the parameters with small random weights
        np.random.seed(1)
        W1 = np.random.randn(n_h,n_x) * 0.01  # Make numbers small
        b1 = np.zeros([n_h, 1])
        W2 = np.random.randn(1,n_h) * 0.01    # Make numbers small
        b2 = np.zeros([n_y,1])
        
        # Just to make sure all components and helper placeholders
        # are in correct format
        assert (W1.shape == (n_h, n_x))
        assert (b1.shape == (n_h, 1))
        assert (W2.shape == (n_y, n_h))
        assert (b2.shape == (n_y, 1))
        
        # Update parameters to dictionary
        parameters = {"W1" : W1,
                           "b1" : b1,
                           "W2" : W2,
                           "b2" : b2}
        return parameters
        

    
    # Initialize the L-parameters with small random floats
    def initializeL(self):
        Z1 = np.random.randn(n_h,n_x) * 0.01
        A1 = np.zeros([n_h, 1])
        Z2 = np.random.randn(1,n_h) * 0.01
        A2 = np.zeros([n_y,1])
        
    
    # THIS IS NOT NEEDED - only for visualising
    # A cost funtion to evaluate development (can use other cost funtions: here cross-entropy)
    def cost(self, A2):
        """
        Computes the cross-entropy cost 
        Arguments:
        A2 ............... The sigmoid output of the second activation, of shape (1, number of examples)
        self.y_train ..... Using the outcome values sized (1, number of examples)

        Returns:
        cost ............. cross-entropy 
        """
    
        m = self.y_train.shape[0]   # number of examples in the training set

        # Compute the cross-entropy cost using A2 from forward()
        logprobs = np.multiply(np.log(A2),self.y_train) + np.multiply(np.log(1-A2),1-self.y_train)
        cost = - (1/m) * np.sum(logprobs)

        cost = np.squeeze(cost)     # makes sure cost is a plain number
                                   
        assert(isinstance(cost, float))
        return cost
       ######################################################################################  
        
     

    def predict(self, x: np.ndarray) -> float:
        """
        Given an example x we want to predict its class probability.
        For example, for the breast cancer dataset we want to get the probability for cancer given the example x.
        :param x: A single example (vector) with shape = (number of features)
        :return: A float specifying probability which is bounded [0, 1].
        """
        #####################################################################################
        # TODO: Implement the forward pass.
        X = x
        print(X)
        print(X.shape)
        
        X = X.reshape(X.shape[0],1)
        
        A2, d = self.forward(X)
        
        predictions = (A2 > 0.5) 
        print("A2 shape: ", A2.shape)
        print("X in def predict shape: ", X.shape)
        print("predictions shape: ", predictions.shape)
        print("Prediction is ", predictions)
        
        return predictions


class TestAssignment5(unittest.TestCase):
    """
    Do not change anything in this test class.

    --- PLEASE READ ---
    Run the unit tests to test the correctness of your implementation.
    This unit test is provided for you to check whether this delivery adheres to the assignment instructions
    and whether the implementation is likely correct or not.
    If the unit tests fail, then the assignment is not correctly implemented.
    """

    def setUp(self) -> None:
        self.threshold = 0.8
        self.nn_class = NeuralNetwork
        self.n_features = 30

    def get_accuracy(self) -> float:
        """Calculate classification accuracy on the test dataset."""
        self.network.load_data()
        self.network.train()

        n = len(self.network.y_test)
        correct = 0
        for i in range(n):
            # Predict by running forward pass through the neural network
            pred = self.network.predict(self.network.x_test[i])
            # Sanity check of the prediction
            assert 0 <= pred <= 1, 'The prediction needs to be in [0, 1] range.'
            # Check if right class is predicted
            correct += self.network.y_test[i] == round(float(pred))
        return round(correct / n, 3)

    def test_perceptron(self) -> None:
        """Run this method to see if Part 1 is implemented correctly."""

        self.network = self.nn_class(self.n_features, False)
        accuracy = self.get_accuracy()
        self.assertTrue(accuracy > self.threshold,
                        'This implementation is most likely wrong since '
                        f'the accuracy ({accuracy}) is less than {self.threshold}.')

    def test_one_hidden(self) -> None:
        """Run this method to see if Part 2 is implemented correctly."""

        self.network = self.nn_class(self.n_features, True)
        accuracy = self.get_accuracy()
        self.assertTrue(accuracy > self.threshold,
                        'This implementation is most likely wrong since '
                        f'the accuracy ({accuracy}) is less than {self.threshold}.')


if __name__ == '__main__':
    unittest.main()
