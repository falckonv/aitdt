from collections import defaultdict

import numpy as np
import copy


class Variable:
    def __init__(self, name, no_states, table, parents=[], no_parent_states=[]):
        """
        name (string): Name of the variable
        no_states (int): Number of states this variable can take
        table (list or Array of reals): Conditional probability table (see below)
        parents (list of strings): Name for each parent variable.
        no_parent_states (list of ints): Number of states that each parent variable can take.

        The table is a 2d array of size #events * #number_of_conditions.
        #number_of_conditions is the number of possible conditions (prod(no_parent_states))
        If the distribution is unconditional #number_of_conditions is 1.
        Each column represents a conditional distribution and sum to 1.

        Here is an example of a variable with 3 states and two parents cond0 and cond1,
        with 3 and 2 possible states respectively.
        +----------+----------+----------+----------+----------+----------+----------+
        |  cond0   | cond0(0) | cond0(1) | cond0(2) | cond0(0) | cond0(1) | cond0(2) |
        +----------+----------+----------+----------+----------+----------+----------+
        |  cond1   | cond1(0) | cond1(0) | cond1(0) | cond1(1) | cond1(1) | cond1(1) |
        +----------+----------+----------+----------+----------+----------+----------+
        | event(0) |  0.2000  |  0.2000  |  0.7000  |  0.0000  |  0.2000  |  0.4000  |
        +----------+----------+----------+----------+----------+----------+----------+
        | event(1) |  0.3000  |  0.8000  |  0.2000  |  0.0000  |  0.2000  |  0.4000  |
        +----------+----------+----------+----------+----------+----------+----------+
        | event(2) |  0.5000  |  0.0000  |  0.1000  |  1.0000  |  0.6000  |  0.2000  |
        +----------+----------+----------+----------+----------+----------+----------+

        To create this table you would use the following parameters:

        Variable('event', 3, [[0.2, 0.2, 0.7, 0.0, 0.2, 0.4],
                              [0.3, 0.8, 0.2, 0.0, 0.2, 0.4],
                              [0.5, 0.0, 0.1, 1.0, 0.6, 0.2]],
                 parents=['cond0', 'cond1'],
                 no_parent_states=[3, 2])
        """
        self.name = name
        self.no_states = no_states
        self.table = np.array(table)
        self.parents = parents
        self.no_parent_states = no_parent_states

        if self.table.shape[0] != self.no_states:
            raise ValueError(f"Number of states and number of rows in table must be equal. "
                             f"Recieved {self.no_states} number of states, but table has "
                             f"{self.table.shape[0]} number of rows.")

        if self.table.shape[1] != np.prod(no_parent_states):
            raise ValueError("Number of table columns does not match number of parent states combinations.")

        if not np.allclose(self.table.sum(axis=0), 1):
            raise ValueError("All columns in table must sum to 1.")

        if len(parents) != len(no_parent_states):
            raise ValueError("Number of parents must match number of length of list no_parent_states.")

    def __str__(self):
        """
        Pretty string for the table distribution
        For printing to display properly, don't use variable names with more than 7 characters
        """
        width = int(np.prod(self.no_parent_states))
        grid = np.meshgrid(*[range(i) for i in self.no_parent_states])
        s = ""
        for (i, e) in enumerate(self.parents):
            s += '+----------+' + '----------+' * width + '\n'
            gi = grid[i].reshape(-1)
            s += f'|{e:^10}|' + '|'.join([f'{e + "("+str(j)+")":^10}' for j in gi])
            s += '|\n'

        for i in range(self.no_states):
            s += '+----------+' + '----------+' * width + '\n'
            state_name = self.name + f'({i})'
            s += f'|{state_name:^10}|' + '|'.join([f'{p:^10.4f}' for p in self.table[i]])
            s += '|\n'

        s += '+----------+' + '----------+' * width + '\n'

        return s

    def probability(self, state, parentstates):
        """
        Returns probability of variable taking on a "state" given "parentstates"
        This method is a simple lookup in the conditional probability table, it does not calculate anything.

        Input:
            state: integer between 0 and no_states
            parentstates: dictionary of {'parent': state}
        Output:
            float with value between 0 and 1
        """
        if not isinstance(state, int):
            raise TypeError(f"Expected state to be of type int; got type {type(state)}.")
        if not isinstance(parentstates, dict):
            raise TypeError(f"Expected parentstates to be of type dict; got type {type(parentstates)}.")
        if state >= self.no_states:
            raise ValueError(f"Recieved state={state}; this variable's last state is {self.no_states - 1}.")
        if state < 0:
            raise ValueError(f"Recieved state={state}; state cannot be negative.")

        table_index = 0
        for variable in self.parents:
            if variable not in parentstates:
                raise ValueError(f"Variable {variable.name} does not have a defined value in parentstates.")

            var_index = self.parents.index(variable)
            table_index += parentstates[variable] * np.prod(self.no_parent_states[:var_index])

        return self.table[state, int(table_index)]


class BayesianNetwork:
    """
    Class representing a Bayesian network.
    Nodes can be accessed through self.variables['variable_name'].
    Each node is a Variable.

    Edges are stored in a dictionary. A node's children can be accessed by
    self.edges[variable]. Both the key and value in this dictionary is a Variable.
    """
    def __init__(self):
        self.edges = defaultdict(lambda: [])  # All nodes start out with 0 edges
        self.variables = {}                   # Dictionary of "name":TabularDistribution

    def add_variable(self, variable):
        """
        Adds a variable to the network.
        """
        if not isinstance(variable, Variable):
            raise TypeError(f"Expected {Variable}; got {type(variable)}.")
        self.variables[variable.name] = variable

    def add_edge(self, from_variable, to_variable):
        """
        Adds an edge from one variable to another in the network. Both variables must have
        been added to the network before calling this method.
        """
        if from_variable not in self.variables.values():
            raise ValueError("Parent variable is not added to list of variables.")
        if to_variable not in self.variables.values():
            raise ValueError("Child variable is not added to list of variables.")
        self.edges[from_variable].append(to_variable)

    def sorted_nodes(self):
        """
        TODO: Implement Kahn's algorithm (or some equivalent algorithm) for putting
              variables in lexicographical topological order.
        Returns: List of sorted variable names.

        Parameters:
        self ......... a bayesian net object

        Returns:
        L ............ a topographic sorted list of which nodes to compute first in a bayesinan net
        
        """

        S = []
        L = []
        ok = True
        
     
        allNodes = len(self.variables)
        edges = copy.deepcopy(self.edges)

        # Nodes without a parent (that is parent is empty)
        for e in edges:
            if (not len(e.parents)):
                #print("Add to S (no edges): \n", e)
                S.append(e)

        #  S is a set of all nodes without ANY incoming edges (has no parents)
        while (len(S) > 0):
            # Take a (random) node from S and move it to L
            n = S.pop()
            #print("Start node popped and added to L is: ", n.name)
            L.append(n)
            currentParent = n.name

            print("\n")
            # Check all nodes in graph if any have a link up to parent = node
            for p, c in edges.items():

                #print("\n")
                #print("Nodes p-name (parent): ", p.name)
                #print("Node c1-name (child 1): "  , c[0].name)
                #if (len(c) > 1):
                #    print("Node c2-name (child 2)", c[1].name)
                    
                #print("Number of parent-parents: "  , len(p.parents))
                #print("Number of childs-parents: "  , len(c[0].parents))
                #print("Is node the same? ", (p.name == currentParent))
                #print("CURRENT PARENT (parent-variable) ", currentParent)

                # If parent is parent itself
                if (p.name == currentParent):
                    #print("\nSkipped loop!")
                    continue

                # Remove all parents and add node to L
                if (len(p.parents) > 0):
                    for i in range(len(p.parents)):
                        #print("Delete edge (p.parent)", p.parents[i])
                        del p.parents[i]
                    #currentParent = p.name
                    L.append(p)

                # Loop through all p's children to see if they have same parent as p (currentParent)
                for j in range(len(c)):

                    if (len(c[j].parents) == 1):
                        del c[j].parents[0]
                        L.append(c[j])

                    if (len(c[j].parents) > 1):
                        # Loop to remove parents if currentParent or p
                        for k in range(len(c[j].parents)):
                            # Remove all edges if parent is currentParent or p
                            if (np.squeeze(c[j].parents[k]) == currentParent or np.squeeze(c[j].parents[k] == p.name)):
                                #print("Delete edge (c.parent)!", c[j].parents[k])
                                del c[j].parents[k]
                        if (len(c[j].parents) == 0):
                            L.append(c[j])

        # Check if any items in L has edges, if so, return error as the graph is cyclical
        for l in L:
            # If any parents is left in edges then the graph is cyclical
            if (len(l.parents)):
                ok = False
                print("Error, graph is cyclical")

        return L, ok              




class InferenceByEnumeration:
    def __init__(self, bayesian_network):
        self.bayesian_network = bayesian_network
        self.topo_order = bayesian_network.sorted_nodes()

    def _enumeration_ask(self, X, evidence):
        pass
        # TODO: Implement Enumeration-Ask algortihm as described in Problem 4 b)

        # Reminder:
        # When mutable types (lists, dictionaries, etc.) are passed to functions in python
        # it is actually passing a pointer to that variable. This means that if you want
        # to make sure that a function doesn't change the variable, you should pass a copy.
        # You can make a copy of a variable by calling variable.copy()
        
    def EnumerateAsk(self, var, e={}):
        # var is a single name of a variable like "A" = the query variable
        # e is carrying all pairs of "A":1  name -value for explored or given variables
        
        # QX is the probability distribution of X for all x_i (variable-values-pair)
        # It could look like [T,T,F,T,T] describing the state (= values as True or False OR the
        # probability values for these "states" - which should sum up to one) of all variables (in topo-order)
        # with a last column with a float giving the particular configurations probability value
        
        # Decision: let all variables be in a dict "name":normalized probability (in a particular configuration)
        # Each row should therefore have a spot for each variable - each row should sum up to 1
        # We populate each row by starting out with first variables two possible states and fork out
        # nodes in the topological order from here
        
        #QX   = defaultdict(lambda: []) 
        #QX   = {} 
        
        assert var not in e, "\nQuery variable should not be in evidence-list"
        
        # Using a list (will fit with the nice-print-out function)
        QX = []      
           
        for i in range(self.bayesian_network.variables[var].no_states):
            # Set states to 0 and 1 ++
            # Add this variable to e
            
            e[var] = i
            # Retreive value given e
            value = self.EnumerateAll(self.topo_order, e)
            # Add values for each state
            QX.append(value)
            #print("\nEnumASK: var is: ", var)
            #print("EnumASK: value is: ", value)
        #print("\nPiplining to normalize\n")
        #print("\nLength of QX is: ", len(QX))
        # Use the normalize for lists when using QX as a list:
        # Normalize() can be used for dictionaries
        return NormalizeList(QX)
    
    # varL can be a single var-name or a list of variables-names
    # TODO: this code only handle ONE parent for a node/variable. Need to change to handle more!
    def EnumerateAll(self, varL, e):
        #print("\n\nStarting EnumerateAll\n")
                
        # When no items is left in the list, return probability 1.0
        if (not len(varL[0])):
            return 1.0
        
        # varL is a list of variables in a bayesian-net (we use the topologically sorted
        newList = copy.deepcopy(varL)
        
        # Get the first varialbe (as a name) from the deep copied varL
        Y = newList[0][0].name
        #print("\nY in newList[0][0] is = \n", Y)

        # Y is the name of the variable. Look for it in the evidence-list e
        if (Y in e):
            #print("\nP(Y) Y is in e: ", e[Y])
            
            # Chech if parent - if so: need to get its default probabilities from the table
            if (not len(self.bayesian_network.variables[Y].parents)):
                # Remove Y which is positioned first in newList
                newList[0].pop(0)
                li = []
                for i in range(self.bayesian_network.variables[Y].no_states):
                    sNP = self.bayesian_network.variables[Y].probability(i, {})
                    li.append(sNP)
                if (self.bayesian_network.variables[Y].no_states == 2):
                    return (self.bayesian_network.variables[Y].probability(li[0], {})) * self.EnumerateAll(newList, e) + (self.bayesian_network.variables[Y].probability(li[1], {})) * self.EnumerateAll(newList, e)
                if (self.bayesian_network.variables[Y].no_states > 2):
                    return (self.bayesian_network.variables[Y].probability(li[0], {})) * self.EnumerateAll(newList, e) + (self.bayesian_network.variables[Y].probability(li[1], {})) * self.EnumerateAll(newList, e) + (self.bayesian_network.variables[Y].probability(li[2], {})) * self.EnumerateAll(newList, e)
            else:
                # Get the first parents name (NB! We ONLY handle ONE parent. If more than one,
                # this code will not work!!
                currentParent = self.bayesian_network.variables[Y].parents[0]
                #print("Variable: ", Y)
                #print("State variable: ", e[Y])
                #print("CurrentParent: ", currentParent)
                #print("State parent: ", e[currentParent])
                # Get Y's parents probability (as it is already in e)
                parentState = e[currentParent]
                #print("Current parent: ", currentParent)
                # Get Y's probability
                PY = self.bayesian_network.variables[Y].probability(e[Y], {currentParent:parentState})
                #print("Variables p-value is: ", PY)
                # Remove Y which is positioned first in newList
                newList[0].pop(0)
                
                return (PY * self.EnumerateAll(newList, e))

        # Y.name is not in e - send all possible values and add Y to e
        else:
            # Remove Y from list
            newList[0].pop(0)
            # TODO: make a nicer implementation    
            # HARDCODED (UGLY!!!)  
            e0 = copy.deepcopy(e)
            e0[Y] = 0
            e1 = copy.deepcopy(e)
            e1[Y] = 1
                
            if (len(self.bayesian_network.variables[Y].parents)):
                cP = self.bayesian_network.variables[Y].parents[0]
                pS = e[cP]
                pYP0 = self.bayesian_network.variables[Y].probability(e0[Y], {cP:pS})
                pYP1 = self.bayesian_network.variables[Y].probability(e1[Y], {cP:pS})
                
                if (self.bayesian_network.variables[Y].no_states > 2):
                    e2 = copy.deepcopy(e)
                    e2[Y] = 2
                    pYP2 = self.bayesian_network.variables[Y].probability(e2[Y], {cP:pS})
                    # If three states
                    return (pYP0*self.EnumerateAll(newList, e0) + pYP1*self.EnumerateAll(newList, e1) + pYP2*self.EnumerateAll(newList, e2))
                
                # If only two binary states and variable has parent
                return (pYP0*self.EnumerateAll(newList, e0) + pYP1*self.EnumerateAll(newList, e1))
            
            # If variable Y has no parents, then assume p=1
            return self.EnumerateAll(newList,e0) + self.EnumerateAll(newList, e1)


    def enumerate_all(self, vars, evidence):
        pass
        # TODO: Implement Enumerate-All algortihm as described in Problem 4 b)

    def query(self, var, evidence={}):
        """
        Wrapper around "_enumeration_ask" that returns a
        Tabular variable instead of a vector
        """
        q = np.array(self.EnumerateAsk(var, evidence)).reshape(-1,1)

        #print("\nFrom query q (should be the same numbers as in the nice print out below): \n", q)

        return Variable(var, self.bayesian_network.variables[var].no_states, q)


# Normalizes a dict-object    
def Normalize(X):
    "Normalize a {key: value} distribution so values sum to 1.0. Mutates dist and returns it."
    total = sum(X.values())
    for key in X:
        X[key] = X[key] / total
        assert 0 <= X[key] <= 1, "Probabilities must be between 0 and 1."
    return X


# Normalizes a list-object    
def NormalizeList(X):
    "Normalize a {key: value} distribution so values sum to 1.0. Mutates dist and returns it."
    total = sum(X)
    for i in range (len(X)):
        X[i] = X[i] / total
        assert 0 <= X[i] <= 1, "Probabilities must be between 0 and 1."
    return X


def problem3c():
    d1 = Variable('A', 2, [[0.8], [0.2]])
    d2 = Variable('B', 2, [[0.5, 0.2],
                           [0.5, 0.8]],
                  parents=['A'],
                  no_parent_states=[2])
    d3 = Variable('C', 2, [[0.1, 0.3],
                           [0.9, 0.7]],
                  parents=['B'],
                  no_parent_states=[2])
    d4 = Variable('D', 2, [[0.6, 0.8],
                           [0.4, 0.2]],
                  parents=['B'],
                  no_parent_states=[2])

    print(f"Probability distribution, P({d1.name})")
    print(d1)

    print(f"Probability distribution, P({d2.name} | {d1.name})")
    print(d2)

    print(f"Probability distribution, P({d3.name} | {d2.name})")
    print(d3)

    print(f"Probability distribution, P({d4.name} | {d2.name})")
    print(d4)

    bn = BayesianNetwork()

    bn.add_variable(d1)
    bn.add_variable(d2)
    bn.add_variable(d3)
    bn.add_variable(d4)
    bn.add_edge(d1, d2)
    bn.add_edge(d2, d3)
    bn.add_edge(d2, d4)

    inference = InferenceByEnumeration(bn)
    
    posterior3 = inference.query('B', {})

    print(f"Probability distribution, P({d2.name})")
    print(posterior3)
    
    posterior4 = inference.query('D', {})

    print(f"Probability distribution, P({d4.name})")
    print(posterior4)
    
    posterior5 = inference.query('C', {'D': 0})

    print(f"Probability distribution, P({d3.name} | {d4.name})")
    print(posterior5)
    
    posterior = inference.query('C', {'D': 1})

    print(f"Probability distribution, P({d3.name} | !{d4.name})")
    print(posterior)
    
    posterior2 = inference.query('B', {'C': 1, 'D': 0})

    print(f"Probability distribution, P({d2.name} | !{d3.name}, {d4.name})")
    print(posterior2)


def monty_hall():
    # TODO: Implement the monty hall problem as described in Problem 4c)
    
    # EXPLAIN MODEL:
    
    # A = contestants first choice of one of three doors (three states each with p=0.333)
    # 0: A=goat_1, 1: A=goat_2, 2:A=car
    # We assume that the numbers are hidden for the contestant
    # The states of A only represent the actual item behind the door
    
    # B = the door of the two remaining doors that will be opened (given A, two states)
    # 0: given not A=car --> open goat_1/goat_2 (the ONE not chosen) --> p=1.0
    # 1: given     A=car --> TWO CHOICES: open goat_1 (A=0) --> p=0.5, open goat_2 (A=1) --> p=0.5
    
    # C = if the contestants change door or not (given A and given B, two states - stay or switch)
    # 0: given B=0  --> stay with first choice (not C), 
    # 1: given B=1  --> change door (C)
    
    # Equally probable that player chooses any of the doors represented
    # by column 0  (goat_1), column 1 (goat_2) and column 2 (car)
    d1 = Variable('A', 3, [[0.3334], [0.3333], [0.3333]])
    
    # Given A (that we behind the sceene "know" if a goat or a car is chosen)
    # Column 0: A=goat_1 chosen, column 1: A=goat_2 and column 2: A=the car
    # B represent the host opening the door with goat_1 (first row) or goat_2(second row)
    d2 = Variable('B', 2, [[1.0, 0.0, 0.5],
                           [0.0, 1.0, 0.5]],
                  parents=['A'],
                  no_parent_states=[3])
    
    # Given A and B, the first row in C is not swaping door, the second is swaping
    d3 = Variable('C', 2, [[0.334, 0.334],
                           [0.666, 0.666]],
                  parents=['B'],
                  no_parent_states=[2])
    
    print(f"Probability distribution, P({d1.name})")
    print(d1)

    print(f"Probability distribution, P({d2.name} | {d1.name})")
    print(d2)

    print(f"Probability distribution, P({d3.name} | {d2.name})")
    print(d3)
    
    bn = BayesianNetwork()

    bn.add_variable(d1)
    bn.add_variable(d2)
    bn.add_variable(d3)
    
    bn.add_edge(d1, d2)
    bn.add_edge(d2, d3)
    
    inference = InferenceByEnumeration(bn)
    
    # The only variable we can manipulate is B (chosen by host) and C (if player will change his/her choice)
    posterior = inference.query('C', {'B': 1})

    print(f"Probability distribution, P({d3.name} | {d2.name}=1)")
    print(posterior)
    
    
    posterior2 = inference.query('C', {'B': 0})

    print(f"Probability distribution, P({d3.name} | {d2.name}=0)")
    print(posterior2)


if __name__ == '__main__':
    problem3c()
    # monty_hall()
